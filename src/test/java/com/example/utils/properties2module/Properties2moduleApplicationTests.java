package com.example.utils.properties2module;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Properties2moduleApplicationTests {

    @Test
    public void contextLoads() {
    }

}
