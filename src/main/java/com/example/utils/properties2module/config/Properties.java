package com.example.utils.properties2module.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aa")
// prefix配置的value 对应properties中属性的第一级.
@Data
public class Properties {

    // 这里配置的属性名 bb，对应properties 中属性的第二级
    private BB bb;

    @Data
    public static class BB{
        // 这里配置的属性名 bb，对应properties 中属性的第三级
        private String frist;
        private String second;
        private String third;

    }
}
