package com.example.utils.properties2module.controller;

import com.example.utils.properties2module.config.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @Autowired
    Properties properties;

    @RequestMapping("/index")
    public String index(){
        // 通过这种方式获取module中的属性名
        final Properties.BB bb = properties.getBb();
        return bb.getFrist();
    }
}
