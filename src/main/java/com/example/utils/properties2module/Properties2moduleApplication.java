package com.example.utils.properties2module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Properties2moduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(Properties2moduleApplication.class, args);
    }
}
